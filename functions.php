<?php


require dirname(__FILE__) . '/Classes/WooSettings.php';

$wooSettings = new WooSettings();


//custom hooks
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
