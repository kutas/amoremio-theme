<?php
class WooSettings {

  protected $unnecessary_billing_fields;
  protected $unnecessary_shipping_fields;
  protected $unnecessary_fields;

  public function __construct(){

      $this->unnecessary_fields = [
        'country',
        //'first_name',
        'last_name',
        'company',
        'address_1',
        'address_2',
        'city',
        'state',
        'postcode',
      ];

      $this->define_hooks();
      $this->enable_theme_support();

  }

  protected function define_hooks(){
      //remove unnecessary fields from checkout page
      add_filter( 'woocommerce_default_address_fields' , [$this,'custom_override_default_address_fields'] );

      //remove items count in sub-categories
      add_filter( 'woocommerce_subcategory_count_html', [$this, 'jk_hide_category_count'] );

      //change add to cart button text for single pages
      add_filter( 'woocommerce_product_single_add_to_cart_text', [$this,'woo_custom_cart_button_text'] );    // 2.1 +


  }

  protected function enable_theme_support(){
    add_theme_support( 'wc-product-gallery-lightbox' );
  }


  function jk_hide_category_count() {
    // No count
  }

  // Our hooked in function - $address_fields is passed via the filter!
  function custom_override_default_address_fields( $address_fields ) {

    foreach ($this->unnecessary_fields as $fieldName) {
      //$address_fields[$fieldName]['required'] = false;
      unset($address_fields[$fieldName]);
    }

       return $address_fields;
  }


  function woo_custom_cart_button_text() {

          return __( 'В Корзину', 'woocommerce' );

  }

}
